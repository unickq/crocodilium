import Core.BasePage;
import Core.BaseTest;
import Core.Elements.Campaign;
import Core.Elements.CampaignType;
import Core.Pages.LoginPage;
import Core.Pages.MainPage;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * User: mchursin
 * Date: 3/2/2016
 * Time: 3:43 PM
 */
public class JustTests extends BaseTest{
    private static String user = "admin";
    private static String pass = "admin123";
    private static String url = "https://dev-test1.mypassbook.nl/mladmin/login?next=/mladmin/campaigns";
    private static MainPage mp;

    @BeforeClass
    public static void setUp() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(url);
        mp = new LoginPage(driver).PerformLogin(user, pass);
    }

    @AfterClass
    public static void tearDown() {
//        driver.quit();
    }

//    @Test
//    public void TestAll(){
//        for (Campaign c : mp.GetCampaigns()) {
//            System.out.println(c);
//        }
//    }

    Date currentDate = new Date();
    @Test
    public void TestPlanned(){
        mp.SwitchTo(CampaignType.planned);
        System.out.println("Verifying planned is after today");
        for (Campaign c : mp.GetCampaigns()) {
            System.out.println(c.getName());
            System.out.println(c.getId());
            Assert.assertTrue(c.getPlannedDate().after(currentDate));
        }
    }

    @Test
    public void TestArchive(){
        mp.SwitchTo(CampaignType.archive);
        System.out.println("Verifying archive is before today");
        for (Campaign c : mp.GetCampaigns()) {
            System.out.println(c.getName());
            System.out.println(c.getId());
            Assert.assertTrue(c.getExpiredDate().before(currentDate));
        }
    }

    @Test
    public void TestActive(){
        mp.SwitchTo(CampaignType.active);
        System.out.println("Verifying planned is before today");
        for (Campaign c : mp.GetCampaigns()) {
            System.out.println(c.getName());
            System.out.println(c.getId());

            Assert.assertTrue(c.getPlannedDate().after(currentDate));
        }
    }


    @Test
    public void TestDraft(){
        mp.SwitchTo(CampaignType.draft);
        System.out.println("Verifying d is before today");

        List<Campaign> drafts = mp.GetCampaigns();

        mp.SwitchTo(CampaignType.active);

        List<Campaign> actives = mp.GetCampaigns();

        StringBuilder sb = new StringBuilder();
        for(Campaign d : drafts){
            for (Campaign a : actives){
                if(d.getId().equals(a.getId())){
                    sb.append(d.getId());
                    sb.append("\n");
                }
            }
        }

        Assert.assertFalse(sb.toString() ,sb.length() > 0);


    }
}
