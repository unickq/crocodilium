package Core.Elements;

/**
 * User: mchursin
 * Date: 3/2/2016
 * Time: 4:40 PM
 */
public enum CampaignType {
    all,
    draft,
    planned,
    active,
    archive
}
