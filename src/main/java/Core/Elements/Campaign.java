package Core.Elements;

import java.util.Date;

/**
 * User: mchursin
 * Date: 3/2/2016
 * Time: 3:59 PM
 */
public class Campaign {
    public Date getCreatedDate() {
        return createdDate;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getPlannedDate() {
        return plannedDate;
    }

    public String getUserCount() {
        return userCount;
    }

    public void setUserCount(String userCount) {
        this.userCount = userCount;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPlannedDate(Date plannedDate) {
        this.plannedDate = plannedDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    private String id;
    private String name;
    private Date plannedDate;
    private Date expiredDate;
    private Date createdDate;
    private String userCount;


    @Override
    public String toString() {
        return "Campaign:" +
                "\n name='" + name + '\'' +
                "\n id='" + id + '\'' +
                "\n createdDate=" + createdDate +
                "\n plannedDate=" + plannedDate +
                "\n expiredDate=" + expiredDate +
                "\n userCount='" + userCount + '\'' +
                "\n----------------------------------------------------";
    }
}

