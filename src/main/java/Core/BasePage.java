package Core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * User: mchursin
 * Date: 3/2/2016
 * Time: 3:39 PM
 */
public class BasePage {
    protected static WebDriver driver;

    protected BasePage(WebDriver driver) {
        BasePage.driver = driver;
        PageFactory.initElements(driver, this);
    }

}
