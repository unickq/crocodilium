package Core.Pages;

import Core.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * User: mchursin
 * Date: 3/2/2016
 * Time: 3:33 PM
 */
public class LoginPage extends BasePage{
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public MainPage PerformLogin(String username, String password){
        edtUsername().sendKeys(username);
        edtPassword().sendKeys(password);
        btnSubmit().click();
        return new MainPage(driver);
    }

    private WebElement edtUsername(){
        return driver.findElement(By.id("id_username"));
    }

    private WebElement edtPassword(){
        return driver.findElement(By.id("id_password"));
    }

    private WebElement btnSubmit(){
        return driver.findElement(By.xpath("//button"));
    }

}
