package Core.Pages;

import Core.BasePage;
import Core.Elements.Campaign;
import Core.Elements.CampaignType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * User: mchursin
 * Date: 3/2/2016
 * Time: 3:58 PM
 */
public class MainPage extends BasePage{
    public MainPage(WebDriver driver) {
        super(driver);
        InitUlCampaigns();
    }

    private By clExpire = By.className("overview-expire");
    private By xpUsers = By.className("overview-amount__users");
    private By xpCreated = By.className("overview-sent");
    private By xpName = By.className("overview-description");

    private List<WebElement> UlsCampaigns;

    private void InitUlCampaigns(){
        UlsCampaigns  = driver.findElements(By.xpath(".//*[@id='messages']/li[*]"));
    }

    public List<Campaign> GetCampaigns(){
        List<Campaign> campaignList = new ArrayList<Campaign>();
        for (WebElement ul: UlsCampaigns){

            Campaign campaign = new Campaign();
            campaign.setId(ul.findElement(By.linkText("View campaign")).getAttribute("href"));
            campaign.setName(ul.findElement(xpName).getText());
            campaign.setExpiredDate(DateParser(ul.findElements(clExpire).get(0).getText()));
            campaign.setPlannedDate(DateParser(ul.findElements(clExpire).get(1).getText()));
            campaign.setCreatedDate(DateParser(ul.findElement(xpCreated).getText()));
            campaign.setUserCount(ul.findElement(xpUsers).getText());
            campaignList.add(campaign);
        }
        return campaignList;
    }


    public MainPage SwitchTo(CampaignType campaignType){
        System.out.println("Switching to " + campaignType);
        driver.findElement(By.className("selectyzeValue")).click();
        switch (campaignType){
            case all:
                driver.findElement(By.xpath("//div/div[1]/div[2]/div//ul/li[1]")).click();
                break;
            case active:
                driver.findElement(By.xpath("//div/div[1]/div[2]/div//ul/li[4]")).click();
                break;
            case archive:
                driver.findElement(By.xpath("//div/div[1]/div[2]/div//ul/li[5]")).click();
                break;
            case draft:
                driver.findElement(By.xpath("//div/div[1]/div[2]/div//ul/li[2]")).click();
                break;
            case planned:
                driver.findElement(By.xpath("//div/div[1]/div[2]/div//ul/li[3]")).click();
                break;
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        InitUlCampaigns();
        return this;
    }


    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    public Date DateParser(String dateInString){
        try {
            dateInString = dateInString.substring(dateInString.indexOf(" ") + 1);
            Date date = formatter.parse(dateInString);
            return date;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
